# 
#
# Author: Samuel Dan
# Contact: samuel.d.dan@gmail.com

# Builder image
FROM alpine:latest as BUILDER

# Install curl
RUN apk --no-cache add \
    curl \

# Download racktables source from Github
    && curl -sSLo /racktables.tar.gz 'https://github.com/RackTables/racktables/archive/refs/tags/RackTables-0.21.5.tar.gz' \
    && tar -xz -C /opt -f /racktables.tar.gz \
    && mv /opt/racktables-RackTables-0.21.5 /opt/racktables.com \
    && rm -f /racktables.tar.gz

FROM php:7.4-fpm as BASE

# Install GD and image extension
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd \

# Install sockets module
    && docker-php-ext-install sockets \

# Install PDO MySQL driver extension
    && docker-php-ext-install mysqli pdo pdo_mysql \

# Install bcmath extension
    && docker-php-ext-install bcmath

# Copy racktables source files
COPY --from=BUILDER /opt/racktables.com /var/www/racktables.com

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["php"]
