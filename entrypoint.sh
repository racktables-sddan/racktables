#!/bin/sh -e

: "${DBNAME:=racktables}"
: "${DBHOST:=mysql}"
: "${DBUSER:=racktables}"

if [ ! -e /var/www/racktables.com/wwwroot/inc/secret.php ]; then
    cat > /var/www/racktables.com/wwwroot/inc/secret.php <<EOF
<?php
\$pdo_dsn = 'mysql:host=${DBHOST};dbname=${DBNAME}';
\$db_username = '${DBUSER}';
\$db_password = '${DBPASS}';
\$user_auth_src = 'database';
\$require_local_account = TRUE;
# See https://wiki.racktables.org/index.php/RackTablesAdminGuide
?>
EOF
fi

chmod 0444 /var/www/racktables.com/wwwroot/inc/secret.php
chown nobody:nogroup /var/www/racktables.com/wwwroot/inc/secret.php

echo 'To initialize the database, first go to /?module=installer&step=5'

exec "$@"
